# Plasma Configuration

This folder contains various KDE Plasma configuration items. Some are my own, and some I just have stashed here in case I ever need to find them again.

Last updated 27 March 2020.

## Contents

- `color-schemes` - Color schemes. To use these, move contents to `~/.local/share/color-schemes/` and select desired color scheme in System Settings.
    - [Breeze Black](https://store.kde.org/p/1358972/) - A very black version of the Breeze color scheme.
    - Breeze Darker - A darker vesion of the built-in Breeze Dark color scheme. I made this one by adjusting Breeze Dark.
    - Breeze Darkest - An even darker version of the built-in Breeze Dark color scheme. I made this one by adjusting Breeze Dark.

- `desktoptheme` - Desktop themes, a.k.a. Plasma style. To use these, move contents to `~/.local/share/desktoptheme/` and select desired Plasma style in System Settings.
    - [Breeze AlphaBlack](https://store.kde.org/p/1084931/) - A semi-transparent black version of the Breeze Dark Plasma style that is configurable using a Plasmoid (see below).

- `plasmoids` - Plasmoids (widgets). To use these, move contents to `~/.local/share/plasma/plasmoids/`. Then right click on the desktop to add widgets.
    - [AlphaBlack Control](https://store.kde.org/p/1237963/) - A widget to adjust the colors and transparency of the Breeze AlphaBlack Plasma style. It is included with the style when downloaded through System Settings.

- `wallpapers` - Desktop wallpapers. To use these, move contents to `~/.local/share/wallpapers/`. Then right click on the desktop to change the wallpaper.
    - tux-grey - A modified version of a wallpaper I found online called [linux-tux-grey](https://www.gnome-look.org/p/1174447/).
    - Path - A wallpaper that ships with Plasma that I particularly like.

- `letter-nav.khotkeys` - My own custom shortcuts for using Alt+[h/j/k/l] as arrow keys (Vim-style). To use these, import into Global Shortcuts in System Settings.
