# change prefix to ctrl-t
unbind C-b
set-option -g prefix C-t
bind-key C-t send-prefix

# enable mouse
set -g mouse on

# advertise self as 256-color
set -g default-terminal "screen-256color"

# vi copy-paste keys
set-window-option -g mode-keys vi
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi 'y' send -X copy-selection

# y in copy mode copies to system clipboard (Linux and MacOS)
if-shell "command -v xclip" "bind-key -T copy-mode-vi 'y' send-keys -X copy-pipe 'xclip -in -selection clipboard'"
if-shell "command -v pbcopy" "bind-key -T copy-mode-vi 'y' send-keys -X copy-pipe 'pbcopy'"

# vi-style pane traversal
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# status bar styling
set-option -g status-left " [#S] "
set-option -g status-left-length 20
set-option -g status-right "#(tmux-mem-cpu-load -g 0 -a 0)  |  %H:%M  %a %d %b  |  $USER@#H "
set-option -g status-right-length 70
set-option -g status-style fg=white,bg=colour237
set-option -g window-status-current-style fg=colour75,bg=default # active window list

# match pane colors to status bar
set-window-option -g pane-active-border-style fg=colour75,bg=default # active pane border
set-window-option -g pane-border-style fg=colour237,bg=colour233 # inactive pane border
set -g window-active-style fg=white,bg=black # active pane fill
set -g window-style fg=white,bg=colour233 # inactive pane fill
