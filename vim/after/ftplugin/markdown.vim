let b:coc_suggest_disable = 1   " disable coc.nvim suggestions
colorscheme jellybeans          " for some reason **bold** text messes up without this
